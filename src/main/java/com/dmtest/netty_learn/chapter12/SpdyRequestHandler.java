package com.dmtest.netty_learn.chapter12;

/**
 * 2018/11/22.
 */
public class SpdyRequestHandler  extends HttpRequestHandler {

    @Override
    protected String getContent() {
        return "This content is transmitted via SPDY\r\n";
    }

}
